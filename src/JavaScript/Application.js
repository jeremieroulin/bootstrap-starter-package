import AbstractApplication from './AbstractApplication';

export default class Application extends AbstractApplication {
	constructor() {
		super();
	}

	start() {
		console.log('started');
	}

	initializePlugins() {
	}
}